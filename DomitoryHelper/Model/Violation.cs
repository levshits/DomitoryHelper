﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;

namespace DomitoryHelper.Model
{
    public class Violation
    {
        public ObjectId Id { get; set; }
        public Student Student { get; set; }
        public DateTime ViolationDate { get; set; }
        public byte AdditionalHours { get; set; }
        public byte AdditionalDuties { get; set; }
    }
}
