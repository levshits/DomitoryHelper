﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Driver;

namespace DomitoryHelper.DAO
{
    class HelperDb : MongoDatabase
    {
        public HelperDb(MongoServer server, MongoDatabaseSettings settings) : base(server, settings)
        {
        }

        public HelperDb(MongoServer server, string name, MongoDatabaseSettings settings) : base(server, name, settings)
        {
        }
    }
}
